import java.util.Comparator;



public class Account {
	private long accountID = 0;
	private double balance = 0;
	public Person owner;
	public Date creationDate;
	private static long numberofAccount = 0;

	public Account(Date creationDate, Person Owner) {
		this.creationDate = creationDate;
		this.owner = Owner;
		this.accountID = this.numberofAccount++;
	}

	public Person getOwner() {
		return this.owner;
	}

	public long getAccountID() {
		return this.accountID;

	}

	public double getBalance() {
		return this.balance;

	}

	public boolean canWithdraw(double amount, Account accountToID) {
		if ((this.balance - amount) >= 0) {
			return true;
		}
		return false;
	}

	public boolean canDeposit(Account accountFromID) {
		if (accountFromID.getAccountID() == this.getAccountID()) {
			return false;
		}
		return true;
	}

	protected boolean deposit(double amount) {
		if (amount > 0) {
			this.balance += amount;
			return true;
		}
		return false;
	}

	protected boolean withdraw(double amount) {

		if ((amount > 0)) {
			this.balance -= amount;
			return true;
		}
		return false;
	}

	public String toString() {
		return "Account(accountID: " + accountID + ", creationDate: " + creationDate + ", owner: " + owner
				+ ", balance: " + balance + ")";
	}
	
	 public static class OwnerNameComparator implements Comparator < Account > {
	        @Override
	        public int compare(Account account1, Account account2) {
	            String name1 = account1.owner.getName();
	            String name2 = account2.owner.getName();
	            int result = name1.compareToIgnoreCase(name2);
	            return result;
	        }
	    }
	     
	    public static class BalanceComparator implements Comparator < Account > {
	        @Override
	        public int compare(Account account1, Account account2) {
	            double balance1 = account1.getBalance();
	            double balance2 = account2.getBalance();
	            if (balance1 < balance2) {
	                return -1;
	            }
	            else if (balance1 > balance2) {
	                return 1;
	            }
	            else {
	                return 0;
	            }
	        }
	    }
	     
	    public static class AccountIdComparator implements Comparator < Account > {
	        @Override
	        public int compare(Account account1, Account account2) {
	            long id1 = account1.getAccountID();
	            long id2 = account2.getAccountID();
	            if (id1 < id2) {
	                return -1;
	            }
	            else if (id1 > id2) {
	                return 1;
	            }
	            else {
	                return 0;
	            }
	        }
	    }
	}