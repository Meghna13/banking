
import java.util.*;

public class Bank {

	private ArrayList<Account> account = new ArrayList<Account>();

	public void addAccount(Account newAccount) {
		this.account.add(newAccount);
	}

	public int getNumberOfAccount() {
		return this.account.size();
	}

	public String toString() {
		return account + "";
	}

	public boolean transfer(long accountFromID, long accountToID, double amount) {
		Account accountToId = null;
		Account accountFromId = null;
		for (int i = 0; i < getNumberOfAccount(); i++) {
			if (account.get(i).getAccountID() == accountFromID) {
				accountFromId = this.account.get(i);
			}
			if (account.get(i).getAccountID() == accountToID) {
				accountToId = this.account.get(i);
			}
		}
		if (accountFromId == null || accountToId == null) {
			return false;
		}
		System.out.println(accountFromId.getAccountID() + " " + accountToId.getAccountID());
		if (checkTransfer(accountFromId, accountToId, amount)) {
			return (accountFromId.withdraw(amount) && accountToId.deposit(amount));
		}
		// if(account.get(i).getbalance()-amount >= account.get(i). )
		return false;
	}

	protected boolean checkTransfer(Account accountFromID, Account accountToID, double amount) {
		if (accountFromID.getAccountID() != accountToID.getAccountID()) {
			if (accountFromID.canWithdraw(amount, accountToID)) {
				if (accountToID.canDeposit(accountFromID)) {

					return true;
				}
			}
		}
		return false;
	}

	 public void printAccountsSorted(int sortingCriteria) {
	        ArrayList < Account > accountsCopy = new ArrayList < > ();
	        accountsCopy.addAll(account);
	         
	        // depending on the input parameter, sort the account by
	        switch (sortingCriteria) {
	            case 1:
	                // balance
	                System.out.println("Sorting accounts by balance:");
	                Collections.sort(accountsCopy, new Account.BalanceComparator());
	                break;
	            case 2:
	                // owner name
	                System.out.println("Sorting accounts by owner name:");
	                Collections.sort(accountsCopy, new Account.OwnerNameComparator());
	                break;
	            case 3:
	                // account ID
	                System.out.println("Sorting accounts by account ID:");
	                Collections.sort(accountsCopy, new Account.AccountIdComparator());
	                break;
	        }
	         
	        for (Account currentAccount: accountsCopy) {
	            System.out.println(currentAccount.toString());
	        }
	         
	        System.out.println("\n");
	    }
	}