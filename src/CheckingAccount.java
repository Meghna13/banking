public class CheckingAccount extends Account {

	private double creditLimit = -1000.0;
	
	public CheckingAccount(Date creationDate, Person Owner, double creditLimit) {
		super(creationDate, Owner);
		this.creditLimit=creditLimit;
	}
	public CheckingAccount(Date creationDate, Person Owner) {
		super(creationDate, Owner);
		this.creditLimit=-1000;
	}
	public boolean canWithdraw(double amount,Account accountToID) {
		if ((this.getBalance()-amount) >= this.creditLimit) {
			return true;
		}
		return false;
	}
}
