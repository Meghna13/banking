public class Date {

	private int day;
	private int month;
	private int year;
	int MonthLastDay;
	boolean Leap;
	private static int Instances;

	public Date() {
		this.day = 1;
		this.month = 1;
		this.year = 1970;
		Instances++;
	}

	public Date(int newDay, int newMonth, int newYear) {
		if (checkDate(newDay, newMonth, newYear)) {
			this.day = newDay;
			this.month = newMonth;
			this.year = newYear;
		} else {
			System.out.println("Wrong date entered, please re-enter ");
		}
		Instances++;
	}

	public Date(Date OtherDate) {
		this.day = OtherDate.day;
		this.month = OtherDate.month;
		this.year = OtherDate.year;
		Instances++;
	}

	public boolean setDate(int Cday, int Cmonth, int Cyear) {
		if (checkDate(Cday, Cmonth, Cyear)) {
			this.day = Cday;
			this.month = Cmonth;
			this.year = Cyear;
			return true;
		} else {
			return false;
		}
	}

	public boolean checkDate(int Cday, int Cmonth, int Cyear) {
		// Checking year
		if (((Cyear % 4 == 0) && (Cyear % 100 == 0) && (Cyear % 400 == 0))
				|| ((Cyear % 4 == 0) && (Cyear % 100 != 0))) {
			// System.out.println("The entered year is a leap year " + Cyear);
			Leap = true;
		} else {
			// System.out.println("The entered year is not a leap year " +
			// Cyear);
			Leap = false;
		}
		// Checking month
		if ((Cmonth == 1) || (Cmonth == 3) || (Cmonth == 5) || (Cmonth == 7) || (Cmonth == 8) || (Cmonth == 10)
				|| (Cmonth == 12)) {
			// System.out.println("The month number is " + Cmonth);
			MonthLastDay = 31;
		} else if ((Cmonth == 4) || (Cmonth == 6) || (Cmonth == 9) || (Cmonth == 11) || (Cmonth == 9)) {
			// System.out.println("The month number is " + Cmonth);
			MonthLastDay = 30;

		} else if (Cmonth == 2 && Leap) {
			// System.out.println("The month number is " + Cmonth);
			MonthLastDay = 29;
		} else if (Cmonth == 2 && !Leap) {
			// System.out.println("The month number is " + Cmonth);
			MonthLastDay = 28;
		} else {
			System.out.println("The entered month is wrong, Please re-enter the month between 1-12");
			return false;
		}
		// checking day
		if (Cday <= MonthLastDay && Cday > 0) {
			// System.out.println("The day is " + Cday);
			return true;
		} else {
			System.out.println("The entered day is wrong, Please re-enter the day between 1 and " + MonthLastDay);
			return false;
		}
	}

    public String toString() {
        String day = (this.day < 10) ? "0" + this.day : "" + this.day;
        String month = (this.month < 10) ? "0" + this.month : "" + this.month;
        return day + "." + month + "." + this.year;
    }


	public boolean isBefore(Date OtherDate) {
		if (this.year < OtherDate.year) {
			return true;
		} else if (this.year == OtherDate.year) {
			if (this.month < OtherDate.month) {
				// System.out.println("YEAR FALSE !!" + this.month);
				// System.out.println("YEAR FALSE !!" + OtherDate.month);
				return true;
			} else if (this.month == OtherDate.month) {
				if (this.day < OtherDate.day) {
					return true;
				} else {
					// System.out.println("DAY FALSE !!");
					return false;
				}
			} else {
				// System.out.println("MONTH FALSE !!");
				return false;
			}
		} else {
			// System.out.println("YEAR FALSE !!");
			return false;
		}
	}

	public Date addDay() {
		Date date = new Date();
		date.day = this.day;
		date.month = this.month;
		date.year = this.year;

		if (date.day < this.MonthLastDay) {
			date.day = this.day + 1;
		} else {
			this.day = 1;
			if (this.month < 12) {
				date.month = this.month + 1;
			} else {
				this.day = 1;
				this.month = 1;
				date.year = this.year + 1;
			}
		}
		return date;
	}

	public static int NumberofInstances() {
		return Instances;
	}
}