import java.util.Comparator;

public class Person {
	private String name;
	private int age;

 public Person(String name, int age){
	 this.name=name;
	 this.age=age;
 }
 
 public String getName(){
	return name;
 }
 
 public int getAge(){
	 return age;
 }
 public String toString() {
     return "Person (name=" + name + ", age=" + age + ")";
 }
 public static class OwnerComparator implements Comparator<Person>{


		@Override
		public int compare(Person person1, Person person2) {
			String name1=person1.getName();
			String name2=person2.getName();
			return name1.compareToIgnoreCase(name2);			
		}	
	
}
}