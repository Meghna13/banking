public class SavingsAccount extends Account {
	public Date fixedDate = null;
	private double creditLimit = 0;

	public SavingsAccount(Date creationDate, Person Owner) {
		super(creationDate, Owner);
	}

	public void fixAccount(Date endDate) {
		this.fixedDate = endDate;
	}

	public boolean canWithdraw(double amount, Account accountToID) {
		if (this.getAccountID() != accountToID.getAccountID()) {
			if ((this.getBalance() - amount) >= creditLimit) {
				if (this.owner.getName().equals(accountToID.owner.getName())) {
					if (fixedDate == null) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public boolean canDeposit(Account accountFromID) {
		Date date = new Date(1, 1, 2000);
		if (accountFromID.getOwner().getName().equals(this.getOwner().getName())) {
			if (this.fixedDate == null) {
				if (!this.creationDate.isBefore(date)) {
					return true;
				}
			}
		}
		return false;
	}
}
