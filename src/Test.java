public class Test{

	public static void main(String[] args) {
		Person person1 = new Person("Hanna", 21);
		Person person2 = new Person("Max", 32);
		Person person3 = new Person("Ivan", 16);
		Person person4 = new Person("Max", 56);
		Date date = new Date();

		CheckingAccount acc1 = new CheckingAccount(date, person1, -2000);
		acc1.deposit(2000);
		CheckingAccount acc2 = new CheckingAccount(date, person2, -2000);
		acc2.deposit(50000);
		CheckingAccount acc3 = new CheckingAccount(date, person3, -2000);
		acc3.deposit(410000);
		CheckingAccount acc4 = new CheckingAccount(date, person4, -2000);
		acc4.deposit(10000);
		SavingsAccount acc5= new SavingsAccount(date, person4);
		acc5.deposit(25000);
		Bank bank = new Bank();
		bank.addAccount(acc1);
		bank.addAccount(acc2);
		bank.addAccount(acc3);
		bank.addAccount(acc4);
		bank.addAccount(acc5);
		bank.printAccountsSorted(1);
		bank.printAccountsSorted(2);
		bank.printAccountsSorted(3);
	}

}